#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>
#include <mcp3004.h>

//BASE is a new pin base for the chip of the analog pins
#define BASE 100
// chose the SPI_channel
#define SPI_CHAN 0


int main(int argc, char** argv){
	// set up the wiringPi library and pins
        if (wiringPiSetupGpio() < 0) {
                fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
                return 1;
        }
	//setup mcp3008 adc
	int mcp_answer, chan;
	if (mcp3004Setup(BASE, SPI_CHAN) < 0) {
                fprintf (stderr, "Unable to setup mcp3008: %s\n", strerror(errno));
                return 1;
        }
	//read values at analog channels
	while(1){
        	for (chan = 0; chan < 8; chan++) {
        		mcp_answer = analogRead(BASE + chan);
			printf("Value at analog channel %i: %d\n", chan, mcp_answer);
        	}
		delay(100);
	}
	return 0;
}
