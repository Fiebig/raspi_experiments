#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>

int pin_led;
int pin_switch;
int led_off = 1;

void switch_event(void){
	if(led_off){ 
		digitalWrite(pin_led, HIGH);
		led_off = 0;
	}
	else{
		digitalWrite(pin_led, LOW);
		led_off = 1;
	}
}

void switch_opened(void){
	digitalWrite(pin_led, LOW);
}

int main(int argc, char** argv){
        if(argc < 3){
                fprintf(stderr, "Usage: %s bcmLedPinNr bcmSwitchPinNr\n", argv[0]);
                return 1;
        }
        pin_led = atoi(argv[1]);
	pin_switch = atoi(argv[2]);
        // set up the wiringPi library and pins
        if (wiringPiSetupGpio() < 0) {
                fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
                return 1;
        }
        pinMode(pin_led, OUTPUT);
        pinMode(pin_switch, INPUT);
	digitalWrite(pin_led, LOW);
	// set up interrupts and handlers
        if (wiringPiISR(pin_switch, INT_EDGE_BOTH, &switch_event) < 0 ) {
                fprintf (stderr, "Unable to setup interrupt: %s\n", strerror(errno));
                return 1;
        }
        //wait for quit
        puts("Enter q to quit");
        int c;
        do {
                c = getchar();
        } while (c != 'q');
	digitalWrite(pin_led, LOW);
        return 0;
}

