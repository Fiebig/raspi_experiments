#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>


int pin_led_red;
int pin_led_yellow;
int pin_led_green;
int pin_switch;


void switch_activated(void){
	//switch to red + yellow
	digitalWrite(pin_led_yellow, HIGH);
	delay(3000);
	//switch to green
	digitalWrite(pin_led_red, LOW);
        digitalWrite(pin_led_yellow, LOW);
	digitalWrite(pin_led_green, HIGH);
	delay(15000);
	//switch to yellow
        digitalWrite(pin_led_green, LOW);
        digitalWrite(pin_led_yellow, HIGH);
	delay(3000);
	//switch back to red
	digitalWrite(pin_led_yellow, LOW);
        digitalWrite(pin_led_red, HIGH);
}

void clear_leds(void){
        digitalWrite(pin_led_red, LOW);
        digitalWrite(pin_led_yellow, LOW);
        digitalWrite(pin_led_green, LOW);
}


int main(int argc, char** argv){
        if(argc < 5){
                fprintf(stderr, "Usage: %s bcmLedRedPinNr bcmLedYellowPinNr bcmLedGreenPinNr bcmSwitchPinNr\n", argv[0]);
                return 1;
        }
        pin_led_red = atoi(argv[1]);
	pin_led_yellow = atoi(argv[2]);
        pin_led_green = atoi(argv[3]);
        pin_switch = atoi(argv[4]);
	// set up the wiringPi library and pins
  	if (wiringPiSetupGpio() < 0) {
      		fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
     	 	return 1;
  	}
        pinMode(pin_led_red, OUTPUT);
        pinMode(pin_led_yellow, OUTPUT);
        pinMode(pin_led_green, OUTPUT);
        pinMode(pin_switch, INPUT);
	clear_leds();
	digitalWrite(pin_led_red, HIGH);
	// set up interrupt and handler
 	if (wiringPiISR(pin_switch, INT_EDGE_RISING, &switch_activated) < 0 ) {
      		fprintf (stderr, "Unable to setup interrupt: %s\n", strerror(errno));
      		return 1;
  	}
	//wait for quit
	puts("Enter q to quit");
	int c;
        do {
    		c = getchar();
  	} while (c != 'q');
	clear_leds();
        return 0;
}

