#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <wiringPi.h>
#include <mcp3004.h>

//BASE is a new pin base for the chip of the analog pins
#define BASE 100
// chose the SPI_channel
#define SPI_CHAN 0

//gpio 18 is the only hardware pwm pin on raspi3
const int pin_led = 18;
int photores_channel;
volatile int stop = 0;


void sig_handler(int signo){
  if (signo == SIGINT)
  	stop = 1;
}


int main(int argc, char** argv){
	if(argc < 2){
                fprintf(stderr, "Usage: %s analog_channelNr\n", argv[0]);
                return 1;
        }
	photores_channel = atoi(argv[1]);
        // set up the wiringPi library and pins
        if (wiringPiSetupGpio() < 0) {
                fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
                return 1;
        }
        //setup mcp3008 adc
        if (mcp3004Setup(BASE, SPI_CHAN) < 0) {
                fprintf (stderr, "Unable to setup mcp3008: %s\n", strerror(errno));
                return 1;
        }
	//setup sigint handler
	if (signal(SIGINT, &sig_handler) == SIG_ERR){
  		fprintf(stderr, "Can't catch SIGINT\n");
		return 1;
	}
	//setup pwm led
	pinMode(pin_led, PWM_OUTPUT);
        pullUpDnControl(pin_led, PUD_DOWN);
        pwmWrite(pin_led, 0);
        //read brightness value at analog channel and set pwm accordingly
	int brightness, pwmValue;
        while(!stop){
                brightness = analogRead(BASE + photores_channel);
		pwmValue = (700-brightness < 0) ? 0 : 700-brightness;
		pwmWrite (pin_led, pwmValue);
                printf("Brightness: %i\n", brightness);
		printf("PWM: %i\n", pwmValue);
                delay(100);
        }
	pwmWrite(pin_led, 0);
        return 0;
}

