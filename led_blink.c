#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>

int main(int argc, char** argv){
	if(argc < 2){
		fprintf(stderr, "Usage: %s bcm_pin_number\n", argv[0]);
		return 1;
	}
	int pin_led = atoi(argv[1]);
	// set up the wiringPi library and pins
        if (wiringPiSetupGpio() < 0) {
                fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
                return 1;
        }
	pinMode(pin_led, OUTPUT);
	int i = 0;
	for(i=0; i<100; i++){
		digitalWrite(pin_led, HIGH);
		delay(100);
		digitalWrite(pin_led, LOW);
		delay(100);
	}
	return 0;
}
