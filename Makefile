CC=gcc
LD_FLAGS=-lwiringPi

all: led_blink led_dim leds_traffic_light led_switch read_adc_mcp3008 photoresistor_pwmled shiftregister_leds_count

led_blink: led_blink.c
	$(CC) $< -o $@ $(LD_FLAGS)

led_dim: led_dim.c 
	$(CC) $< -o $@ $(LD_FLAGS)

leds_traffic_light: leds_traffic_light.c 
	$(CC) $< -o $@ $(LD_FLAGS)

led_switch: led_switch.c
	$(CC) $< -o $@ $(LD_FLAGS)

read_adc_mcp3008: read_adc_mcp3008.c 
	$(CC) $< -o $@ $(LD_FLAGS)

photoresistor_pwmled: photoresistor_pwmled.c 
	$(CC) $< -o $@ $(LD_FLAGS)

shiftregister_leds_count: shiftregister_leds_count.c 
	$(CC) $< -o $@ $(LD_FLAGS)

clean:
	rm -f led_blink led_dim leds_traffic_light led_switch read_adc_mcp3008 photoresistor_pwmled shiftregister_leds_count
