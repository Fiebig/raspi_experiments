#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <unistd.h>

// chose the SPI_channel
const int spi_channel = 0;
volatile int stop = 0;

void sig_handler(int signo){
  if (signo == SIGINT)
        stop = 1;
}

int main(int argc, char** argv){
	// set up the wiringPi library and pins
        if (wiringPiSetupGpio() < 0) {
                fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
                return 1;
        }
        //setup spi
	int fd;
        if ((fd = wiringPiSPISetup (spi_channel, 2000000)) < 0) {
                fprintf (stderr, "Unable to setup SPI: %s\n", strerror(errno));
                return 1;
        }
        //setup sigint handler
        if (signal(SIGINT, &sig_handler) == SIG_ERR){
                fprintf(stderr, "Can't catch SIGINT\n");
                return 1;
        }
	pinMode(11, OUTPUT);
        //setup pwm led
        unsigned char pattern = 0;
	while(!stop){
		wiringPiSPIDataRW(spi_channel, &pattern, 1);
		pattern >>= 1;
		//delay(1000);
	}
	write(fd, &pattern, 8);
	return 0;
}

