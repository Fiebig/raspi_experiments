#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>

//gpio 18 is the only hardware pwm pin on raspi3
const int pin_led = 18;

int main (int argc, char** argv){
  	// set up the wiringPi library and pins
        if (wiringPiSetupGpio() < 0) {
                fprintf (stderr, "Unable to setup wiringPi: %s\n", strerror(errno));
                return 1;
        }
  	pinMode(pin_led, PWM_OUTPUT);
	pullUpDnControl(pin_led, PUD_DOWN);
	//pwmSetClock(2);
  	pwmWrite(pin_led, 0);
	//dim led from dark to bright
	int pwmValue;
	for (pwmValue=0; pwmValue<=1024; pwmValue++){
		pwmWrite (pin_led, pwmValue);
		delay(10);
	}
	pwmWrite(pin_led, 0);
	return 0;
}
