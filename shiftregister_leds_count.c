#include <stdio.h>
#include <wiringPi.h>
#include <sr595.h>

int main (int argc, char** argv){
  int i, bit ;
  wiringPiSetup();

  //Pin base 100 for 8 pins.
  //Use wiringPi pins 0, 1 & 2 for data, clock and latch
  sr595Setup (100, 8, 0, 1, 2);

  printf ("Raspberry Pi - Shift Register Count\n") ;
  for (i = 0 ; i < 256 ; ++i){
    for (bit = 0 ; bit < 8 ; ++bit)
      digitalWrite (100 + bit, i & (1 << bit));
    //
    printf("Displayed number: %i\n", i);
    delay(50);
  }
  for (i=0; i<8; i++)
      digitalWrite (100 + i, 0);
  return 0;
}
